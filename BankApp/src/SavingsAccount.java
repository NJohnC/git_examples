
public class SavingsAccount extends BankAppMain {
	
	
	public SavingsAccount(String bankName, String accHolderName) {
		super(bankName, accHolderName);
	}

	public long startAmount = 10_000l;
	
	
	@Override
	public void withdrawAmount(double amount) {
		double balance = startAmount - amount;
		if(balance < 10000) {
			System.out.println("Cannot withdraw amount. Contact the bank manager");
		}
		else {
			startAmount-=amount;
			System.out.println("Amount Withdrawn is: "+amount);
			System.out.println("Available Balance is: "+startAmount);
		}
		
	}

	@Override
	public void depositAmount(double amount) {
		if(amount >= 50000) {
			System.out.println("Cannot add money in the account more than Rs 50000");
		}else {
			startAmount+=amount;
			System.out.println("Amount Deposited is: "+amount);
			System.out.println("Available Balance is: "+startAmount);
		}
		
	}


}
