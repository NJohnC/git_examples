
public abstract class BankAppMain {
	
	private String bankName;
	private String accHolderName;
	
	public abstract void withdrawAmount(double amount);
	public abstract void depositAmount(double amount);
	
	public BankAppMain(String bankName, String accHolderName) {
		this.bankName = bankName;
		this.accHolderName = accHolderName;
	}
	
	@Override
	public String toString() {
		return bankName+" "+accHolderName;
	}
}