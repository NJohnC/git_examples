
public class BankMainTest {

	public static void main(String[] args) {
		
		
		BankAppMain savAccount = new SavingsAccount("HDFC_Savings", "Sameer");
		BankAppMain currAccounts = new CurrentAccounts("HDFC_Current", "Sameer");
		BankAppMain salAccounts = new SalariedAccounts("HDFC_Salary", "Sameer");
		
		System.out.println("SAVING ACCOUNT DETAILS");
		System.out.println(savAccount);
		savAccount.depositAmount(20000);
		savAccount.withdrawAmount(1435);
		savAccount.withdrawAmount(230000);
		System.out.println();
		
		System.out.println("CURRENT ACCOUNT DETAILS");
		System.out.println(currAccounts);
		currAccounts.depositAmount(20000.00);
		currAccounts.withdrawAmount(1000.00);
		System.out.println();
		
		System.out.println("SALARIED ACCOUNT DETAILS");
		System.out.println(salAccounts);
		salAccounts.depositAmount(32999.88);
		salAccounts.withdrawAmount(1212.00);
		salAccounts.withdrawAmount(12121212);
		System.out.println();
		
	}

}
