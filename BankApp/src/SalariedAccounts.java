
public class SalariedAccounts extends BankAppMain {
	
	public SalariedAccounts(String bankName, String accHolderName) {
		super(bankName, accHolderName);
	}

	public long startAmount = 0;

	@Override
	public void withdrawAmount(double amount) {
		double balance = startAmount - amount; 
		if(balance<0) {
			System.out.println("Cannot have negative");
		}
		
		else {
			startAmount -= amount;
			System.out.println("Amount Withdrawn is: " + amount);
			System.out.println("Available Balance is: " + startAmount);
		}
	}

	@Override
	public void depositAmount(double amount) {
	
			startAmount += amount;
			System.out.println("Amount Deposited is: " + amount);
			System.out.println("Available Balance is: " + startAmount);

	}
}
