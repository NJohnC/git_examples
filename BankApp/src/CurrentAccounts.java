
public class CurrentAccounts extends BankAppMain {
	
	
	public CurrentAccounts(String bankName, String accHolderName) {
		super(bankName, accHolderName);
	}

	public long startAmount = 25_000l;

	@Override
	public void withdrawAmount(double amount) {
		double balance = startAmount - amount;
		if (balance <= 25000) {
			System.out.println("Cannot withdraw amount. Contact the bank manager");
		} else {
			startAmount -= amount;
			System.out.println("Amount Withdrawn is: " + amount);
			System.out.println("Available Balance is: " + startAmount);
		}

	}

	@Override
	public void depositAmount(double amount) {
		
			startAmount += amount;
			System.out.println("Amount Deposited is: " + amount);
			System.out.println("Available Balance is: " + startAmount);

	}
}
