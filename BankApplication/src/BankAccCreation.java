public class BankAccCreation {

	private String name;
	private static long startFlag = 1000l;
	private long accountId;

	BankAccCreation(String name) {
		this.name = name;
		this.accountId = startFlag++;
	}

	public String getName() {
		return name;
	}

	public long getAccountId() {
		return accountId;
	}
}