package com.hexaware.exceptions;

import java.util.*;

public class CastVoteDemo {

	public static void main(String[] args) {
		Scanner sc = new Scanner (System.in);
		System.out.println("Enter your age");
		int age = sc.nextInt();
		try {
			validateAge(age);
		} catch (InvalidAgeException e) {
			System.out.println(e.getMessage());
		}
		sc.close();
	}
	
	private static void validateAge(int age) throws InvalidAgeException{
		if(age<18)
		{
			throw new InvalidAgeException ("Too Young");
		}
		else
			System.out.println("Eligible");
	}
}
