package com.hexaware.exceptions;

import java.util.*;

public class Throw {
	
	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Enter your age");
		
		int Age = sc.nextInt();
		
		try {
			if(Age < 18)
			{
				throw new IllegalArgumentException(Integer.toString(Age));
			}
		} catch(IllegalArgumentException exception)
		{
			System.out.println("Not eligible for Voting");
		}
		if(Age>18)
		{
			System.out.println("Eligible for casting vote");
		}
	}
}
