package com.hexaware.exceptions;

import java.util.*;

public class ExceptionDemo {
		
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter a positive number");
		int divident = sc.nextInt();
		
		System.out.println("Enter a divisor");
		int divisor = sc.nextInt();
		float result=0;
		try {
		result = (divident/divisor);
		}
		catch(ArithmeticException exception) {
		System.out.println("Enter a value greater than 0");
		}
		
		if(result > 0) {
			System.out.println("Result after division is "+ result);
		}
		sc.close();
	}
}
