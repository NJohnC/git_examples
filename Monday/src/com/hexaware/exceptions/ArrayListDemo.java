package com.hexaware.exceptions;

import java.util.*;

public class ArrayListDemo {
	
	public static void main (String[] args) {
		List<Integer> list = new LinkedList<>();
		list.add(34);
		list.add(44);
		list.add(55);
		list.add(66);
		list.add(77);
		list.add(88);
	
		System.out.println(list);
		
		Integer value = list.get(4);
		System.out.println("Value of index 4"+value);
		
		boolean contains = list.contains(77);
		System.out.println("List contains 77 "+contains);
		
		list.clear();
		System.out.println("Size of list :: "+list.size());
		
	}
	
}
