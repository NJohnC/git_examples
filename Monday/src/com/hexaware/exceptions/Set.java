package com.hexaware.exceptions;

import java.util.Iterator;
import java.util.HashSet;
import java.util.Set;

public class Set {
	
		public static void main (String[] args) {
			Set<BankAccount> accounts = new Hashset<>();
			accounts.add(new BankAccount(1234, "Harish",20_000));
			accounts.add(new BankAccount(1235, "Harish",20_000));
			accounts.add(new BankAccount(1236, "Harish",20_000));
			accounts.add(new BankAccount(1237, "Harish",20_000));
			accounts.add(new BankAccount(1238, "Harish",20_000));
			accounts.add(new BankAccount(1239, "Harish",20_000));
		}
		Iterative<BankAccount> it = accounts.iterator();
		while(it.hasNext()) {
			System.out.println(it.next());
		}
}
