
public class DoctorClient {

	public static void invoke(DoctorAppMain dc) {
		dc.treatPatients();
	}
	
	
	public static void main(String[] args) {
//		DoctorAppMain d = new DoctorAppMain();
		DoctorAppMain d1 = new OrthoDocMain();
		DoctorAppMain d2 = new DentistDocMain();
		DoctorAppMain d4 = d2;
		OrthoDocMain d5 = new OrthoDocMain();
		DentistDocMain d6 = new DentistDocMain();
		
		d5.takeAppointment();
		d1.treatPatients();
		d6.takeAppointment();
		//d2.treatPatients();
		DoctorAppMain d7 = d1;
		OrthoDocMain d8 = (OrthoDocMain) d7;
		
		d8.conductCTScan();
		
		//d.treatPatients();
		d1.treatPatients();
		//d2.treatPatients();
		d4.treatPatients();
		
		//DoctorAppMain doctor = new DoctorAppMain();
		DoctorAppMain ortho = new OrthoDocMain();
		DoctorAppMain dentist = new DentistDocMain();
		//invoke(doctor);
		invoke(ortho);
		invoke(dentist);
		
		
		
		
		
	}
}
