
public class OrthoDocMain extends DoctorAppMain {
	
	@Override
	public void treatPatients() {
		System.out.println("Treat the Patient. Appoint an Orthopedic");
	}
	
	public void takeAppointment() {
		System.out.println("To take an appointment call 00000001111");
	}
	
	public void conductCTScan() {
		System.out.println("Conducting CT Scan");
	}
	
	public void conductXRay() {
		System.out.println("Conducting X-Ray");
	}
	
}
