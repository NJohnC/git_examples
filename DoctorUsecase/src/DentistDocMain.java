
public class DentistDocMain extends DoctorAppMain {
	
	
	@Override
	public void treatPatients() {
		System.out.println("Treat the Patient. Appoint an Dentist");
		
	}
	
	public void treatKids() {
		System.out.println("Treat Kids....");
	}
	
	
	public void takeAppointment() {
		System.out.println("Take appointment. Call 0119897654");
	}
	
	public void checkTeeth() {
		System.out.println("Proper check up of teeth");
	}
	
	public void doOperation() {
		System.out.println("Do opertion if problem found");
	}
	
}
