
public abstract class DoctorAppMain {
	
	private String doctorName;
	private int doctorId;
	private int yearsOfExperience;
	
	public abstract void treatPatients();
	
}
