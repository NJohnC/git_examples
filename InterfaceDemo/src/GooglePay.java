
public class GooglePay implements PaymentGateway, MobileRecharge {

	@Override
	public void transact(String from, String to, double amount, String remarks) {
		System.out.printf("Initiating Payments from %s to %s for Rupees %f. Notes: %s", from,to,amount,remarks);
	}

	@Override
	public void mobileRec(long mobNumber, double amount) {
		System.out.printf("Mobile Number %d is recharged with %f", mobNumber, amount);
	}

}
