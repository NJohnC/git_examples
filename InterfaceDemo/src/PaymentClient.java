import java.util.Scanner;

public class PaymentClient {

	public static void main(String[] args) {
		
		
		Scanner sc = new Scanner(System.in);
		System.out.println("Please enter the option:");
		System.out.println("1-->Google Pay:");
		System.out.println("2-->PhonePe");
		int option = sc.nextInt();
		
		PaymentGateway p = null;
		
		
		switch(option) {
			case 1: 
				p = new GooglePay();
				break;
			case 2: 
				p = new PhonePe();
				break;
			default:
				System.out.println("Payment Option not available");
				break;
				
		}
		
		
		if (p != null) {
			p.transact("Sameer", "Sam", 10000.98, "Transaction Done");
		}
		
		MobileRecharge m = new GooglePay();
		m.mobileRec(9097667894l, 1000);
		sc.close();
	}

}
