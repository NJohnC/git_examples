
public class PhonePe implements PaymentGateway {

	@Override
	public void transact(String from, String to, double amount, String remarks) {
		System.out.printf("Initiating Payments from %s to %s for Rupees %f. Notes: %s", from,to,amount,remarks);
		System.out.println();
		System.out.println("Rewards points gained: "+50);
	}

}
